#ifndef TESTS_H
# define TESTS_H
# include "so_long.h"
# include <stdio.h>

bool	is_map_with_valid_name(void);
bool	is_map_with_invalid_name_1(void);
bool	is_map_with_invalid_name_2(void);
bool	is_map_with_invalid_name_3(void);
bool	is_map_with_invalid_name_4(void);
bool	is_map_with_invalid_name_5(void);
bool	is_map_with_invalid_name_6(void);
bool	is_map_with_invalid_name_7(void);
bool	is_map_with_invalid_name_8(void);
bool	is_map_with_invalid_name_9(void);


#endif
